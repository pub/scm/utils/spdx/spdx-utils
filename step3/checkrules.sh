#!/bin/bash

../lcheck.py --no_spdx --noconflicts --exclude='Documentation,LICENSES' --dropmodule --dropexport --license='GPL-2.0-only,Linux-OpenIB' --dual_license -f rules master.json >rules.txt

mkdir -p rules
rm -f rules/*

../splitrules.py template.txt rules.txt


