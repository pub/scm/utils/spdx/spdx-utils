#!/bin/sh

# Start from the root of the spdx directory which needs to
# be outside of the kernel tree.

# Hand in the path to the Kernel tree
export LINUXDIR=`realpath $1`
SPDXDIR=`pwd`

# Step 1
#
# Fixup the following files which have been omitted in the first
# sweep:
#
# Build files
# Files which have only a MODULE_LICENSE() entry
# Files which have only a EXPORT_SYMBOL() entry
#
# Scan the tree
cd $LINUXDIR
git rebase --abort
git checkout master
$SPDXDIR/scan $SPDXDIR/step1
cd $SPDXDIR

# Create the patches for build files
cd step1/build
./genmake.sh
cd $SPDXDIR

# Create the patches for modules-only
cd step1/modules-only
./genmod.sh
cd $SPDXDIR

# Create the patches for export symbol only
cd step1/export-symbol
./genexp.sh
cd $SPDXDIR

# Go and apply the mess
cd $LINUXDIR

git branch -D step1
git checkout -b step1

rm patches
ln -s $SPDXDIR/step1/build/patches patches
git quiltimport
rm patches
ln -s $SPDXDIR/step1/modules-only/patches patches
git quiltimport
rm patches
ln -s $SPDXDIR/step1/export-symbol/patches patches
git quiltimport
rm patches
cd $SPDXDIR

# Step 2
#
# Handle the first round of GPLV2-only/or-later which do not have SPDX and no
# other license and no conflicts.
#
# Re-scan the tree
cd $LINUXDIR
$SPDXDIR/scan $SPDXDIR/step2
cd $SPDXDIR

cd step2
./checkrules.sh
./genreplace.sh
cd $SPDXDIR

cd $LINUXDIR

git branch -D step2
git checkout -b step2

rm patches
ln -s $SPDXDIR/step2/patches patches
git quiltimport
rm patches
cd $SPDXDIR

# Step 3
#
# Handle the OpenIB mess
#
# Re-scan the tree
cd $LINUXDIR
$SPDXDIR/scan $SPDXDIR/step3
cd $SPDXDIR

cd step3
./checkrules.sh
./genreplace.sh
cd $SPDXDIR

cd $LINUXDIR

git branch -D step3
git checkout -b step3

rm patches
ln -s $SPDXDIR/step3/patches patches
git quiltimport
rm patches
cd $SPDXDIR

#
# More fun to come
#
# Re-scan the tree
cd $LINUXDIR
$SPDXDIR/scan $SPDXDIR/step4
cd $SPDXDIR
