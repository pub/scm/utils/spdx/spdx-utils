#!/bin/bash

PD=patches
mkdir -p $PD
rm $PD/*

find rules/ -type f >patchrules.lst

../lcheck.py --no_spdx --exclude='Documentation,LICENSES' --dropmodule --dropexport -u --flat -f patch_boiler --license='GPL-2.0-only,GPL-2.0-or-later' --patchdir=$PD --ruleslist=patchrules.lst --source=$LINUXDIR master.json


