#!/bin/bash

../lcheck.py --no_spdx --noconflicts --exclude='Documentation,LICENSES' --dropmodule --dropexport -u --license='GPL-2.0-only,GPL-2.0-or-later' -f rules master.json >rules.txt

mkdir -p rules
rm -f rules/*

../splitrules.py template.txt rules.txt
