Subject: treewide: Replace GPLv2 boilerplate/reference with SPDX - rule $RULE
Date:
From:

Based on $NRPATTERNS normalized pattern(s):

$PATTERNS

extracted by the scancode license scanner the SPDX license identifier

$SPDXID

has been chosen to replace the boilerplate/reference in $NRFILES file(s).

The result has been manually verified and compared against a license
compliance dataset provided by ....

Signed-off-by:
