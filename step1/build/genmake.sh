#!/bin/sh

PD=patches
mkdir -p $PD
rm $PD/*

../../lcheck.py --no_license --exclude='Documentation,LICENSES' --filter='Makefile,Kconfig' --flat -f patch_none -t header-make-none.txt --patchname=build-none.patch --patchdir=$PD --source=$LINUXDIR ../master.json

