#!/bin/bash

PD=patches

mkdir -p $PD
rm -f $PD/*

../../lcheck.py --dropexport --module_only --exclude='Documentation,LICENSES' -u --license='GPL-2.0-only' --flat -f patch_module -t header-module-none.txt --source=$LINUXDIR  --patchname=modules-none.patch --patchdir=$PD ../master.json

