#!/usr/bin/env python
# SPDX-License-Identifier: GPL-2.0-only
# Copyright Thomas Gleixner <tglx@linutronix.de>

from argparse import ArgumentParser, REMAINDER
import sys
import os

if __name__ == '__main__':

    parser = ArgumentParser(description='Split rules file')
    parser.add_argument('template', metavar='template',
                        help='Commit message template')
    parser.add_argument('rulesfile', metavar='rulesfile',
                        help='Rules file to split up')

    args = parser.parse_args()

    template = open(args.template).readlines()
    lines = open(args.rulesfile).readlines()

    rulenr = 0
    rulename = None
    spdx = None
    patterns = []
    nrpatterns = 0
    nrfiles = 0
    inpattern = False
    infiles = False

    try:
        os.mkdir('rules')
    except:
        pass

    for line in lines:
        l = line.strip()
        if l.startswith('Rule:'):
            if rulenr:
                try:
                    fd = open('rules/%04d-%s' %(rulenr, rulename), 'w')
                    for tl in template:
                        tl = tl.replace('$RULE', '%d' %rulenr)
                        tl = tl.replace('$NRPATTERNS', '%d' %nrpatterns)
                        tl = tl.replace('$NRFILES', '%d' %nrfiles)

                        if tl.startswith('$PATTERNS'):
                            fd.writelines(patterns)
                        else:
                            fd.write(tl)
                    fd.close()
                except Exception, ex:
                    print(ex)
                    sys.exit(1)

            rulenr += 1
            rulename = l.split(':')[1].strip()
            spdx = None
            patterns = []
            nrpatterns = 0
            nrfiles = 0
            inpattern = False
            infiles = False
            continue

        elif not inpattern and not infiles:
            if l.startswith('Files:'):
                nrfiles = int(l.split(':')[1].strip())
            elif l.startswith('Patterns'):
                nrpatterns = int(l.split(':')[1].strip())
                inpattern = True
            continue

        elif inpattern:
            if l.startswith('Filenames:'):
                inpattern = False
                infiles = True
            else:
                patterns.append(line)
            continue

        else:
            continue

